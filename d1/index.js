/* JSON Objects
  - stands for javascript object notation
  - notation 
    - JSON is used for serializing different data types into bytes (binary 0,1)
    - serialization 
     - converting data into a series of bytes for easier transmission/transfer of information 
     -  byte = binary digits (1 and 0) that is used to represent a character 
  
  JSON DATA Format 
  - syntax: 
      {
        "propertyA" : "valueA",
        "propertyB" : "valueB",
      }
    
*/
/* 
  JS Object 
  {
    city: "QC",
    province: "Metro Manila",
    country: "Philippines"
  }

  JSON Objects have "string" keys whereas JS Objects do not
  JSON
  {
    "city": "QC",
    "province": "Metro Manila",
    "country": "Philippines"
  }

  JSON ARRAY
"cities": [
  {
    'city': "QC",
    'province': "Metro Manila",
    'country': "Philippines"
  },
  {
    'city': "QC",
    'province': "Metro Manila",
    'country': "Philippines"
  },
  {
    'city': "QC",
    'province': "Metro Manila",
    'country': "Philippines"
  }
]
*/

/* 
  JSON Methods
  - JSON contains methods for parsing and converting data into stringified JSON
    - stringified JSON 
      - javascript object converted into a string to be used in other functions of a JavaScript application 
      - information has been converted to a string in JSON

  - stringify method 
    - used to convert JS objects into JSON (string)
    - upon sending data from client to server 
    - JSON.stringify(#)
*/

let batchesArray = [
  {
    batchName: "Batch X",
  },
  {
    batchName: "Batch Y",
  },
];

console.log(`Result from stringify method`);
console.log(JSON.stringify(batchesArray));

let data = JSON.stringify({
  name: "John",
  age: 31,
  address: {
    city: "Manila",
    country: "Philippines",
  },
});
console.log(data);

// let fName = prompt("What's your first name?");
// let lName = prompt("What's your last name?");
// let age = prompt("How old are you?");
// let city = prompt("What city do you live in?");
// let country = prompt("What country do you live in?");
// let zipCode = prompt("What is your zip code?");

// let info = JSON.stringify({
//   fName: fName,
//   lName: lName,
//   age: age,
//   address: {
//     city: city,
//     country: country,
//     zipCode: zipCode,
//   },
// });
// console.log(info);

/* 
  Converting stringified JSON to JS objects
  - information is commonly sent to applications in stringified JSON and then converted back into objects 
  - this happens for both sending information to a backend app and sending information back to frontend app 
  - upon receiving data, JSON text can be converted to a JS obejct with parse 
*/
let batchesJSON = `[
  {
    "batchName": "Batch X"
  },
  {
    "batchName": "Batch X"
  }
]`;
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `
{
  "name": "John",
  "age" : "31",
  "address":{
    "city": "Manila",
    "country": "Philippines"
  }
}`;
console.log(JSON.parse(stringifiedObject));

/* 
  stringify: client to server; so the server will understand the data 
  parse: server to client  
*/
